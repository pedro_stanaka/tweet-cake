--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.11
-- Dumped by pg_dump version 9.1.11
-- Started on 2014-02-17 15:16:07 BRT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1940 (class 1262 OID 809790)
-- Name: blog_teste; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE blog_teste WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'pt_BR.UTF-8' LC_CTYPE = 'pt_BR.UTF-8';


\connect blog_teste

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 169 (class 3079 OID 11684)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1943 (class 0 OID 0)
-- Dependencies: 169
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 162 (class 1259 OID 877917)
-- Dependencies: 5
-- Name: groups; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE groups (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- TOC entry 161 (class 1259 OID 877915)
-- Dependencies: 162 5
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1944 (class 0 OID 0)
-- Dependencies: 161
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- TOC entry 168 (class 1259 OID 877953)
-- Dependencies: 5
-- Name: posts; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE posts (
    id bigint NOT NULL,
    title character varying(150) NOT NULL,
    body text NOT NULL,
    user_id integer,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- TOC entry 167 (class 1259 OID 877951)
-- Dependencies: 168 5
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1945 (class 0 OID 0)
-- Dependencies: 167
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE posts_id_seq OWNED BY posts.id;


--
-- TOC entry 166 (class 1259 OID 877940)
-- Dependencies: 5
-- Name: tweets; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tweets (
    id bigint NOT NULL,
    body character varying(150) NOT NULL,
    user_id integer,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- TOC entry 165 (class 1259 OID 877938)
-- Dependencies: 166 5
-- Name: tweets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tweets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1946 (class 0 OID 0)
-- Dependencies: 165
-- Name: tweets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tweets_id_seq OWNED BY tweets.id;


--
-- TOC entry 164 (class 1259 OID 877925)
-- Dependencies: 5
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    password character(40) NOT NULL,
    group_id integer NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- TOC entry 163 (class 1259 OID 877923)
-- Dependencies: 164 5
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1947 (class 0 OID 0)
-- Dependencies: 163
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 1810 (class 2604 OID 877920)
-- Dependencies: 162 161 162
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);


--
-- TOC entry 1813 (class 2604 OID 877956)
-- Dependencies: 168 167 168
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY posts ALTER COLUMN id SET DEFAULT nextval('posts_id_seq'::regclass);


--
-- TOC entry 1812 (class 2604 OID 877943)
-- Dependencies: 165 166 166
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tweets ALTER COLUMN id SET DEFAULT nextval('tweets_id_seq'::regclass);


--
-- TOC entry 1811 (class 2604 OID 877928)
-- Dependencies: 163 164 164
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 1929 (class 0 OID 877917)
-- Dependencies: 162 1936
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: -
--

COPY groups (id, name, created, modified) FROM stdin;
1	Administrator	2014-02-17 09:33:52	2014-02-17 09:33:52
2	Publishers	2014-02-17 09:34:11	2014-02-17 09:34:11
\.


--
-- TOC entry 1948 (class 0 OID 0)
-- Dependencies: 161
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('groups_id_seq', 2, true);


--
-- TOC entry 1935 (class 0 OID 877953)
-- Dependencies: 168 1936
-- Data for Name: posts; Type: TABLE DATA; Schema: public; Owner: -
--

COPY posts (id, title, body, user_id, created, modified) FROM stdin;
\.


--
-- TOC entry 1949 (class 0 OID 0)
-- Dependencies: 167
-- Name: posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('posts_id_seq', 1, false);


--
-- TOC entry 1933 (class 0 OID 877940)
-- Dependencies: 166 1936
-- Data for Name: tweets; Type: TABLE DATA; Schema: public; Owner: -
--

COPY tweets (id, body, user_id, created, modified) FROM stdin;
\.


--
-- TOC entry 1950 (class 0 OID 0)
-- Dependencies: 165
-- Name: tweets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('tweets_id_seq', 1, false);


--
-- TOC entry 1931 (class 0 OID 877925)
-- Dependencies: 164 1936
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY users (id, username, password, group_id, created, modified) FROM stdin;
1	Pedro	525954e98defe6dac21194d9f6171831e2dfed1e	1	2014-02-17 09:36:46	2014-02-17 09:36:46
\.


--
-- TOC entry 1951 (class 0 OID 0)
-- Dependencies: 163
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('users_id_seq', 1, true);


--
-- TOC entry 1815 (class 2606 OID 877922)
-- Dependencies: 162 162 1937
-- Name: groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- TOC entry 1823 (class 2606 OID 877961)
-- Dependencies: 168 168 1937
-- Name: posts_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- TOC entry 1821 (class 2606 OID 877945)
-- Dependencies: 166 166 1937
-- Name: tweets_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tweets
    ADD CONSTRAINT tweets_pkey PRIMARY KEY (id);


--
-- TOC entry 1817 (class 2606 OID 877930)
-- Dependencies: 164 164 1937
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 1819 (class 2606 OID 877932)
-- Dependencies: 164 164 1937
-- Name: users_username_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- TOC entry 1826 (class 2606 OID 877962)
-- Dependencies: 164 1816 168 1937
-- Name: posts_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 1825 (class 2606 OID 877946)
-- Dependencies: 164 166 1816 1937
-- Name: tweets_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tweets
    ADD CONSTRAINT tweets_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 1824 (class 2606 OID 877933)
-- Dependencies: 164 162 1814 1937
-- Name: users_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_group_id_fkey FOREIGN KEY (group_id) REFERENCES groups(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 1942 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-02-17 15:16:07 BRT

--
-- PostgreSQL database dump complete
--

