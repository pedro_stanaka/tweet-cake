<?php
/**
 * TweetFixture
 *
 */
class TweetFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'body' => array('type' => 'string', 'null' => false, 'length' => 150),
		'user_id' => array('type' => 'integer', 'null' => true),
		'created' => array('type' => 'datetime', 'null' => true),
		'modified' => array('type' => 'datetime', 'null' => true),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'body' => 'Lorem ipsum dolor sit amet',
			'user_id' => 1,
			'created' => '2014-02-14 16:38:14',
			'modified' => '2014-02-14 16:38:14'
		),
	);

}
