<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	var $layout = "bootstrap3";

	public $components = array(
		'Session',
		'Auth' => array(
        	'authenticate' => array(
	            'Form' 
        	),
        	'authError' => 'Did you really think you are allowed to see that?',
        	'loginRedirect' => array('controller' => 'Tweets', 'action' => 'index'),
            'logoutRedirect' => array('controller' => 'Users', 'action' => 'login'),
            'flash'=>array('element'=>'flash_error', 'key'=>'auth')
    	)
	);


	/**
	 * beforeFilter callback
	 *
	 * @return void
	 */
		public function beforeFilter() {
			parent::beforeFilter();
			$this->Auth->allow('view','index');

			$userId = $this->Auth->user('id');
			if (isset($userId)) {
				$this->loadModel('User');
				$this->set('currentUser', $this->User->find('first', array(
					'conditions'=>array(
							'User.id' => $this->Auth->user('id')
						),
					'contain' => array('Group')
					))
				);
			}
		}
}
