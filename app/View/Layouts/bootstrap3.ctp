<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>
		BoostCake -
		<?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<?php 
		echo $this->Html->css(array('vendor/normalize', 'vendor/bootstrap.min.css', 'main'));
		echo $this->Html->script('vendor/modernizr-2.6.2-respond-1.1.0.min');
		echo $this->Html->script(array('vendor/jquery-1.10.1.min', 'vendor/bootstrap.min', 'main'), array('block'=>'scriptBottom'));
	?>

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<?php
	echo $this->fetch('meta');
	echo $this->fetch('script');
	echo $this->fetch('css');
	?>
</head>

<body>
	<div class='container-full'>
		<div class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<?php echo $this->Html->link('Micro Blog', array(
						'controller' => 'Tweets',
						'action' => 'index'
						), array('class' => 'navbar-brand')); ?>
				</div>

				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav">
						<li><?php echo $this->Html->link('Tweets', array('controller'=>'Tweets',
							'action' => 'index'
							)); ?></li>
					</ul>
					<?php if (isset($currentUser) and count($currentUser, COUNT_RECURSIVE)): ?>
						<div class="user-info pull-right">
							<ul class="nav navbar-nav">
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $currentUser['User']['username'] ?><b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a href="#">Group: <?php echo $currentUser['Group']['name'] ?></a></li>
											<li><?php echo $this->Html->link('Sign Out', array('controller'=>'Users', 'action' => 'logout')); ?></li>
										</ul>
									</li>
							</ul>
						</div> <!-- .user-info -->
					<?php endif; ?>
				</div>
			</div>
		</div>

				<div class="container-fluid">
					<?php echo $this->Session->flash(); ?>
					<?php echo $this->fetch('content'); ?>
				</div><!-- /container -->

	<!-- Le javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<?= $this->fetch('scriptBottom'); ?>
</div>
</body>
</html>
