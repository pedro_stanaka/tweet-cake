<div class="users form">

	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1><?php echo __('Sign Up'); ?></h1>
			</div>
		</div>
	</div>
	<div class="row">
		
		<div class="col-md-9">
			<?php echo $this->Form->create('User', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('username', array('class' => 'form-control', 'placeholder' => 'Username'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('password', array('class' => 'form-control', 'placeholder' => 'Password'));?>
				</div>
				<?php if (isset($isAdmin) && $isAdmin === true): ?>
					<?php echo $this->Form->input('group_id', array('class' => 'form-control', 'placeholder' => 'Group Id'));?>
				<?php else: ?>
					<?php echo $this->Form->input('group_id', array('type'=>'hidden'));?>
				<?php endif; ?>
				<p></p>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Sign Up'), array('class' => 'btn btn-primary')); ?>
				</div>

			<?php echo $this->Form->end() ?>

		</div><!-- end col md 12 -->
	</div><!-- end row -->
</div>
