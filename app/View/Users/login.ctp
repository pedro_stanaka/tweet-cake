<div class="row">
	<div class="container">
	<?php echo $this->Session->flash('auth'); ?>
		<?php echo $this->Form->create('User', array('role'=>'form')); ?>
		<fieldset>
			<div class="form-group row">
				<?php echo $this->Form->label('username', 'User name'); ?>
				<?php echo $this->Form->input('username', array('class'=>'form-control col-md-8', 'label'=>false));	 ?>
			</div>
			<div class="form-group row">
				<?php 	
				echo $this->Form->label('password', 'Password');
				echo $this->Form->input('password', array('class'=>'form-control col-md-8', 'label'=>false));	
				?>	
			</div>

		</fieldset>

		<?php echo $this->Form->end(array('label'=>'Login', 'class'=>'btn btn-primary', 'div'=>array('class'=>'row'))); ?>
	</div>
</div>